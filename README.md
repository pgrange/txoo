# txoo

## What is txoo?

`txoo` is an oracle for the Bitcoin network that produces attestations to the chain tip and to the outputs spent in each block.
It uses GCS filters similar to BIP-158 compact block filters.

The oracle is useful for Lightning Network devices that need to verify the on-chain state of a channel without direct
access to the Bitcoin network.  This is especially useful for secure enclaves without network access.

`no_std` support is provided.

## Security model

The oracle attests to the chain tip and to the outputs spent in each block at a specific time.

The outputs spent are summarized by a Golomb-coded set (GCS) filter representing the set of all outpoints in transaction
inputs in the block.
The attestation includes a "filter header", which hashes the current GCS filter as well as the previous filter header.
This allows the client to verify the entire filter history has not been tampered with.

The proof of spend/non-spend includes the spending transactions, either in an SPV proof, or a full block if there
is a filter false-positive.

## Running txood

```shell
cargo run --package txood --release 
```

or if you have `bitcoind` running on the same machine, use this for the initial run,
which will be much faster (~45 minutes on a fast machine):

```shell
cargo run --package txood --release -- --preload
``` 

check the last 256 attestations:

```shell
cargo run --package txoo-cli --release -- check
```

a quick local test starting at a recent block:

```shell
cargo run --package txood --release -- -d data --start-block 760000
```

Note that the filter headers won't match if you don't start at genesis, because it covers all previous
blocks' filters.

## Check an oracle

To check our testnet oracle, ensure `bitcoind --testnet` is running and:

```shell
cargo run -p txoo-cli -- -n testnet -s https://vls.tech/txoo/testnet/ check
```

## Run coverage

```shell
cargo install cargo-kcov
./scripts/run-kcov
```
        
View Coverage Report

[target/kcov/cov/index.html](target/kcov/cov/index.html)

## Benchmark

```shell
cd txoo-bench
cargo bench
```

## TODO

 - [x] emit attestations
 - [x] prune
 - [x] sign
 - [x] SPV proofs for spent outputs
 - [ ] consider using multiple GCS filters with different sizes, so that we can have a small filter for applications that just watch a few outpoints, and a large filter for applications that watch a lot of outpoints
