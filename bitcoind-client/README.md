# bitcoind-client

A client for the Bitcoin Core RPC interface, written in Rust.

Also provides chain following and SPV and TXOO proof generation.
