use crate::bitcoind_client::{BlockHeaderData, BlockSourceResult};
use crate::follower::Tracker;
use crate::BlockSource;
use async_trait::async_trait;
use bitcoin::hashes::Hash;
use bitcoin::{
    Block, BlockHash, BlockHeader, OutPoint, PackedLockTime, Transaction, TxMerkleNode, Txid,
};
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

pub(crate) fn make_block(n: u32, prev_blockhash: BlockHash) -> Block {
    let mut block = Block {
        header: BlockHeader {
            version: 0,
            prev_blockhash,
            merkle_root: TxMerkleNode::all_zeros(),
            time: 0,
            bits: 0,
            nonce: 0,
        },
        txdata: vec![Transaction {
            version: 0,
            lock_time: PackedLockTime(n),
            input: vec![],
            output: vec![],
        }],
    };
    block.header.merkle_root = block.compute_merkle_root().expect("merkle root");
    block
}

pub(crate) struct Chain {
    blocks: HashMap<BlockHash, Block>,
    height: u32,
    tip: BlockHash,
    genesis_hash: BlockHash,
}

#[derive(Clone)]
pub(crate) struct DummyBlockSource {
    chain: Arc<Mutex<Chain>>,
}

impl DummyBlockSource {
    pub(crate) fn new() -> Self {
        let genesis = make_block(0, BlockHash::all_zeros());
        let mut blocks = HashMap::new();
        let hash = genesis.block_hash();
        blocks.insert(hash, genesis);
        let chain = Chain {
            blocks,
            height: 0,
            tip: hash,
            genesis_hash: hash,
        };
        Self {
            chain: Arc::new(Mutex::new(chain)),
        }
    }

    pub(crate) fn genesis_hash(&self) -> BlockHash {
        self.chain.lock().unwrap().genesis_hash
    }

    pub(crate) fn add(&mut self) -> BlockHeader {
        let mut chain = self.chain.lock().unwrap();
        let block = make_block(chain.height + 1, chain.tip);
        chain.tip = block.block_hash();
        let header = block.header.clone();
        chain.blocks.insert(block.block_hash(), block);
        chain.height += 1;
        header
    }

    pub(crate) fn remove(&mut self) {
        let mut chain = self.chain.lock().unwrap();
        let block = chain.blocks.get(&chain.tip).unwrap();
        chain.tip = block.header.prev_blockhash;
        chain.height -= 1;
    }

    pub(crate) fn get_block_sync(&self, header_hash: &BlockHash) -> Block {
        let chain = self.chain.lock().unwrap();
        chain.blocks.get(header_hash).unwrap().clone()
    }
}

#[async_trait]
impl BlockSource for DummyBlockSource {
    async fn get_header(&self, header_hash: &BlockHash) -> BlockSourceResult<BlockHeaderData> {
        let chain = self.chain.lock().unwrap();
        let block = chain.blocks.get(header_hash).unwrap();
        return Ok(BlockHeaderData {
            header: block.header.clone(),
            height: 0,
            chainwork: Default::default(),
        });
    }

    async fn get_block(&self, header_hash: &BlockHash) -> BlockSourceResult<Block> {
        let chain = self.chain.lock().unwrap();
        Ok(chain.blocks.get(header_hash).unwrap().clone())
    }

    async fn get_block_hash(&self, height: u32) -> BlockSourceResult<Option<BlockHash>> {
        let chain = self.chain.lock().unwrap();
        if height > chain.height {
            return Ok(None);
        }
        let mut cur_hash = chain.tip;
        let mut cur_height = chain.height;
        while cur_height > height {
            let block = chain.blocks.get(&cur_hash).unwrap();
            cur_height -= 1;
            cur_hash = block.header.prev_blockhash;
        }
        Ok(Some(cur_hash))
    }

    async fn get_best_block(&self) -> BlockSourceResult<(BlockHash, u32)> {
        let chain = self.chain.lock().unwrap();
        return Ok((chain.tip, chain.height));
    }
}

pub(crate) struct DummyTracker();

#[async_trait]
impl Tracker for DummyTracker {
    async fn forward_watches(&self) -> (Vec<Txid>, Vec<OutPoint>) {
        (vec![], vec![])
    }

    async fn reverse_watches(&self) -> (Vec<Txid>, Vec<OutPoint>) {
        (vec![], vec![])
    }
}
